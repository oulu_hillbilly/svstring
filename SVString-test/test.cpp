#include "pch.h"
#include "SVString.h"

class SVString_test : public ::testing::Test {
protected:
    SVString str;
    void SetUp(){
        str = SVString("test");
    }

    void TearDown() {

    }

};

class SVString_testosterone : public ::testing::Test {
protected:
    SVString str;
    void SetUp() {
        str = SVString("testosterone");
    }

    void TearDown() {

    }
};

TEST(SVString_length, SV_test) {
    SVString str("test");
    size_t l = str.length();
    ASSERT_EQ(l, 4);
}

TEST(SVString_length, SV_apple) {
    SVString str("apple");
    size_t l = str.length();
    ASSERT_EQ(l, 5);
}

TEST(SVString_length, SV_varanka) {
    SVString str("varanka");
    size_t l = str.length();
    ASSERT_EQ(l, 7);
}
TEST_F(SVString_test, CanGetCString) {
    const char* cstr = str.get_cstr();
    ASSERT_STREQ(cstr, "test");
}

TEST_F(SVString_test, CanCopyContruct) {
    SVString str2(str);
    str.destroy();
    ASSERT_STREQ(str2.get_cstr(),"test");
}

TEST_F(SVString_test, FindFirst_t) {
    int ndx = str.findFirst("t");
    ASSERT_EQ(ndx, 0);
}

TEST_F(SVString_test, FindFirst_e) {
    int ndx = str.findFirst("e");
    ASSERT_EQ(ndx, 1);
}

TEST_F(SVString_test, FindLast_t) {
    int ndx = str.findLast("t");
    ASSERT_EQ(ndx, 3);
}

TEST_F(SVString_test, FindLast_s) {
    int ndx = str.findLast("s");
    ASSERT_EQ(ndx, 2);
}

TEST_F(SVString_test, CanAppendToString) {
    str.append("o");
    const char* str2 = str.get_cstr();
    ASSERT_STREQ(str2, "testo");
}

TEST_F(SVString_test, CanConcatString) {
    str.concat("case");
    ASSERT_STREQ(str.get_cstr(), "testcase");
}

TEST_F(SVString_test, CanConcatString2) {
    str.concat("osterone");
    ASSERT_STREQ(str.get_cstr(), "testosterone");
}

TEST_F(SVString_testosterone, CanGetSubstring_esto) {
    auto substr = str.substring(1,4);
    ASSERT_STREQ(substr->get_cstr(), "esto");
}

TEST_F(SVString_testosterone, CanGetSubstring_osterone) {
    auto substr = str.substring(4,SVString::npos);
    ASSERT_STREQ(substr->get_cstr(),"osterone");
}

TEST(SVString_split, SplitCommaRetArr3) {
    SVString str("apple,banana,carrot");
    auto parts = str.split(",");
    ASSERT_EQ(parts.size(), 3);    
}

TEST(SVString_split, SplitCommaRetArr3ContainStrings) {
    SVString str("apple,banana,carrot");
    auto parts = str.split(",");
    ASSERT_STREQ(parts[0]->get_cstr(), "apple");
    ASSERT_STREQ(parts[1]->get_cstr(), "banana");
    ASSERT_STREQ(parts[2]->get_cstr(), "carrot");
}

TEST(SVString_split, SplitUnderScoreRetArr4) {
    SVString str("dog_giraffe_elephant_cat");
    auto parts = str.split("_");
    ASSERT_EQ(parts.size(), 4);
}

TEST(SVString_split, SplitUnderScoreRetArr4ContainStrings) {
    SVString str("dog_giraffe_elephant_cat");
    auto parts = str.split("_");
    ASSERT_STREQ(parts[0]->get_cstr(), "dog");
    ASSERT_STREQ(parts[1]->get_cstr(), "giraffe");
    ASSERT_STREQ(parts[2]->get_cstr(), "elephant");
    ASSERT_STREQ(parts[3]->get_cstr(), "cat");
}

TEST(SVString_split, SplitNoDelimeterRetOriginal) {
    SVString str("dog_giraffe_elephant_cat");
    auto parts = str.split("|");
    ASSERT_STREQ(parts[0]->get_cstr(), "dog_giraffe_elephant_cat");
}

TEST(SVString_replace, ReplaceCharAtIndex) {
    SVString str("dogs&cats");
    str.replace('|', 4);
    ASSERT_STREQ(str.get_cstr(), "dogs|cats");
}

TEST(SVString_replace, ReplaceBatmanBadman) {
    SVString str("Batman");
    str.replace('d', str.findFirst("t"));
    ASSERT_STREQ(str.get_cstr(), "Badman");
}

TEST(SVString_replace, ReplaceAllAE) {
    SVString str("Badman");
    str.replaceAll('a', 'e');
    ASSERT_STREQ(str.get_cstr(), "Bedmen");
}

TEST(SVString_replace, ReplaceAllEI) {
    SVString str("testosterone");
    str.replaceAll('e','i');
    ASSERT_STREQ(str.get_cstr(), "tistostironi");
}

TEST(SVString_upper_lower, ToUpper) {
    SVString str("sami");
    str.toUpper();
    ASSERT_STREQ(str.get_cstr(), "SAMI");
}

TEST(SVString_upper_lower, ToLower) {
    SVString str("SAMI");
    str.toLower();
    ASSERT_STREQ(str.get_cstr(), "sami");
}