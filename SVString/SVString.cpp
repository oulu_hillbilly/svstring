#include "SVString.h"
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cctype>

SVString::SVString() {
    DPRINT("default");
}

SVString::SVString(const char* cstr) {
    DPRINT("ctor with 1 param");
    size_t l = length(cstr);

    _strbuff = static_cast<char*>(std::malloc(sizeof(char)*(l + 1)));
    std::memcpy(_strbuff, cstr, l);
    _strbuff[l] = 0;
    _strlen = l;
}

SVString::SVString(char *cstr) {
    DPRINT("ctor with 1 non-const param");
    size_t l = length(cstr);

    _strbuff = cstr;
    _strlen = l;
}

SVString::SVString(const SVString& other) {
    DPRINT("copy ctor");
    size_t size = length(other._strbuff);
    _strbuff = static_cast<char*>(std::malloc(sizeof(char)*(size + 1)));
    std::memcpy(_strbuff, other._strbuff, size);
    _strbuff[size] = 0;
    _strlen = size;
}

SVString::SVString(SVString&& other) noexcept {
    DPRINT("move ctor");
    _strbuff = other._strbuff;
    _strlen = other._strlen;
    other._strbuff = nullptr;
    other._strlen = 0;
}

SVString::~SVString() noexcept {
    DPRINT("dtor");
    destroy();
}

SVString& SVString::operator=(const SVString& other) {
    DPRINT("copy assignment");
    size_t size = length(other._strbuff);
    _strbuff = static_cast<char*>(std::malloc(sizeof(char)*(size + 1)));
    std::memcpy(_strbuff, other._strbuff, size);
    _strbuff[size] = 0;
    _strlen = size;
    return *this;
}

SVString& SVString::operator=(SVString&& other) noexcept {
    DPRINT("move assignment");
    _strbuff = other._strbuff;
    _strlen = other._strlen;
    other._strbuff = nullptr;
    other._strlen = 0;
    return *this;
}

void SVString::destroy() {
    DPRINT("destroy");
    if (_strbuff != nullptr) {
        std::free(_strbuff);
        _strbuff = nullptr;
    }
}

int SVString::findFirst(const char* c) {
    int ndx = 0;
    while (ndx < _strlen) {
        if (_strbuff[ndx] == *c)
            return  ndx;
        ++ndx;
    }
    return npos;
}

int SVString::findLast(const char* c) {
    int ndx = _strlen - 1;
    while (ndx > -1) {
        if (_strbuff[ndx] == *c)
            return ndx;
        --ndx;
    }
    return npos;
}

void SVString::append(const char* c) {
    _strbuff = static_cast<char*>(std::realloc(_strbuff, sizeof(char)*(_strlen + 2)));
    _strbuff[_strlen] = *c;
    ++_strlen;
    _strbuff[_strlen] = 0;
}

void SVString::concat(const char* str) {
    size_t l = _strlen + length(str);
    _strbuff = static_cast<char*>(std::realloc(_strbuff, sizeof(char)*(l + 1)));
    std::memcpy(&_strbuff[_strlen], str, length(str));
    _strbuff[l] = 0;
}

SVString::SVString_Shared_ptr SVString::substring(int start, int end) {
    if (end >= _strlen || end == npos) { end = _strlen; }
    int buffLen = (end - start) + 1;
    char* buff = static_cast<char*>(std::malloc(sizeof(char)*(buffLen + 1)));
    std::memcpy(buff, &(_strbuff[start]), sizeof(char)*buffLen);
    buff[buffLen] = 0;

    SVString substr(buff);
    auto ptr = make_SVString_shared_ptr(substr);
    _ptrs.push_back(ptr);
    return ptr;
}

SVString::SVString_shared_ptr_container SVString::split(const char* delimeter) {
    SVString_shared_ptr_container ptrs;
    int start = 0, end = 0;
    while (end != npos) {
        end = findInRange(delimeter, start);
        if (end != npos) {
            ptrs.push_back(substring(start, end - 1));
            start = end + 1;
        }
        else {
            ptrs.push_back(substring(start, end));
        }
    }
    return ptrs;
}

void SVString::replace(char c, int index) {
    _strbuff[index] = c;
}

void SVString::replaceAll(char r, char c) {
    for (size_t i = 0; i < _strlen; ++i) {
        if (_strbuff[i] == r)
            _strbuff[i] = c;
    }
}

void SVString::toUpper() {
    for (size_t i = 0; i < _strlen; ++i) {
        _strbuff[i]=std::toupper(_strbuff[i]);
    }
}

void SVString::toLower() {
    for (size_t i = 0; i < _strlen; ++i) {
        _strbuff[i] = std::tolower(_strbuff[i]);
    }
}

// Private
size_t SVString::length(const char* cstr) {
    size_t l = 0;
    while (cstr[l] != '\0') {
        ++l;
    }
    DPRINT_I(l);
    return l;
}

int SVString::findInRange(const char* c, size_t start) {
    auto rangeEnd = _strlen;
    for (size_t i = start; i < rangeEnd; ++i) {
        if (_strbuff[i] == *c) {
            return i;
        }
    }
    return npos;
}