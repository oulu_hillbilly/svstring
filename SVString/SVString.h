#ifndef __SVSTRING_H
#define __SVSTRING_H

//#define _SVSTRING_DEBUG

#ifdef _SVSTRING_DEBUG
#include <cstdio>
#define DPRINT(msg)             printf("%s: %s\n",msg,__func__)
#define DPRINT_I(num)           printf("%s: %d\n", __func__,num)
#else 
#define DPRINT
#define DPRINT_I
#endif // _MYDEBUG

#include <memory>
#include <vector>

/**************************************************************
 FEATURES
  - find first char /
  - find last char  /
  - substring  /
  - split string /
  - concatenate string /
  - append char to string /
  - replace character /
  - replace all occurences of character /
  - to upper
  - to lower
 
 WBN
  - get rid of STL vector 
****************************************************************/


class SVString {
    using SVString_ptr = SVString*;
    using SVString_Shared_ptr = std::shared_ptr<SVString>;
    using SVString_shared_ptr_container = std::vector<SVString::SVString_Shared_ptr>;

public:
    const static int npos = -1;
    SVString();
    explicit SVString(char* cstr);
    explicit SVString(const char* cstr);

    ~SVString() noexcept;

    SVString(const SVString& other);
    SVString(SVString&& other) noexcept;

    SVString& operator=(const SVString& other);
    SVString& operator=(SVString&& other) noexcept;

    void destroy();

    inline const char* get_cstr() const { return _strbuff; }
    inline size_t length() { return _strlen; }

    int findFirst(const char* c);
    int findLast(const char* c);

    void append(const char* c);
    void concat(const char* str);
    SVString_Shared_ptr substring(int start, int end);
    SVString_shared_ptr_container split(const char* delimeter);

    void replace(char c, int index);
    void replaceAll(char r, char c);

    void toUpper();
    void toLower();
private:
    char* _strbuff = nullptr;
    size_t _strlen = 0;
    SVString_shared_ptr_container _ptrs;

    size_t length(const char* cstr);
    int findInRange(const char* c, size_t start);

    inline SVString_Shared_ptr make_SVString_shared_ptr(SVString SVStr) { return std::make_shared<SVString>(SVStr); }
};
#endif //!__SVSTRING_H
